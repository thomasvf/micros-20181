#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "ggpio.h"

int ggpio_open(GPIO *gpio, char *file, int mode)
{
    int modestd;
    int fd;

    if(mode == READ_ONLY) {
        modestd = O_RDONLY;
    } else if(mode == WRITE_ONLY) {
        modestd = O_WRONLY;
    } else if(mode == READ_WRITE) {
        modestd = O_RDWR;
    } else {
        return -1;
    }

    if((fd = open(file, modestd)) < 0){
        return -1;
    }

    gpio->fd = fd;
    gpio->mode = mode;
    
    return 0;
}

int ggpio_write(GPIO gpio, int value)
{
    if(gpio.mode == READ_ONLY) return -1;
    if(value != GPIO_HIGH && value != GPIO_LOW) return -1;

    char c;
    if(value == GPIO_HIGH) c = '1';
    else c = '0';

    lseek(gpio.fd, 0, SEEK_SET);
    if(write(gpio.fd, &c, sizeof(c)) < 0) {
        return -1;
    }

    return 0;
}

int ggpio_read(GPIO gpio)
{
    if(gpio.mode == WRITE_ONLY) return -1;

    char c;
    lseek(gpio.fd, 0, SEEK_SET);
    if(read(gpio.fd, &c, sizeof(c)) < 0) {
        return -1;
    }

    if(c == '0') return GPIO_LOW;
    return GPIO_HIGH;
}

int ggpio_close(GPIO gpio)
{
    close(gpio.fd);

    return 0;
}