#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "gpwm.h"

static int pwm_period_all = -1;
static int set_default_pwm_period();

int gpwm_open(PWM *pwm, int which)
{
    if(pwm_period_all == -1) set_default_pwm_period();
    if(which != 1 && which != 3 && which != 5 && which != 7 && which != 9 && which != 11){
        printf("Invalid which.\n");
        return -1;
    }

    int fd_dc, fd_enable;

    char buff[50];
    snprintf(buff, 50, "/sys/class/pwm/pwmchip0/pwm%d/duty_cycle", which);
    if((fd_dc = open(buff, O_WRONLY)) < 0) {
        printf("Error opening duty cycle file.\n");
        return -1;
    }

    snprintf(buff, 50, "/sys/class/pwm/pwmchip0/pwm%d/enable", which);
    if((fd_enable = open(buff, O_WRONLY)) < 0) {
        printf("Error opening enable file.\n");
        return -1;
    }

    pwm->fd_duty_cycle = fd_dc;
    pwm->fd_enable = fd_enable;

    gpwm_disable((*pwm));
    return 0;
}

int gpwm_write_duty_cycle(PWM pwm, int duty_cycle)
{
    if(duty_cycle > pwm_period_all) return -1;

    char buff[10];
    snprintf(buff, 10, "%d", duty_cycle);

    lseek(pwm.fd_duty_cycle, 0, SEEK_SET);
    if(write(pwm.fd_duty_cycle, buff, strlen(buff)) < 0) {
        return -1;
    }

    return 0;
}

int gpwm_write_dc_percent(PWM pwm, int percent){
    if(percent < 0 || percent > 100) return -1;

    int duty_cycle = (int) percent * pwm_period_all / 100;
    return gpwm_write_duty_cycle(pwm, duty_cycle);
}

int gpwm_write_pwm_period(int pwm_period)
{
    int fd;
    char buff[100];
    snprintf(buff, 100, "/sys/class/pwm/pwmchip0/device/pwm_period");
    
    if((fd = open(buff, O_WRONLY)) < 0) {
        return -1;
    }

    char buff_period[10];
    snprintf(buff_period, 10, "%d", pwm_period);
    if(write(fd, buff_period, strlen(buff_period)) < 0) {
        return -1;
    }

    return 0;
}

int gpwm_enable(PWM pwm)
{
    if(write(pwm.fd_enable, "1\0", 2) < 0) return -1;
    return 0; 
}

int gpwm_disable(PWM pwm)
{
    if(write(pwm.fd_enable, "0\0", 2) < 0) return -1;
    return 0;
}

int gpwm_close(PWM pwm)
{
    gpwm_disable(pwm);

    close(pwm.fd_enable);
    close(pwm.fd_duty_cycle);

    return 0;
}

static int set_default_pwm_period()
{
    if(gpwm_write_pwm_period(PWM_DEFAULT_PERIOD) < 0) return -1;
    pwm_period_all = PWM_DEFAULT_PERIOD;
    return 0;
}
