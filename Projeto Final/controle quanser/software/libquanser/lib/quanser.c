#include <stdio.h>

#include <quanser.h>

#define PI 3.14159265

int quanser_open(QUANSER *quanser)
{
    DECODER decoder;
    GPIO decoder_en;
    PWM motor;
    GPIO motor_en;
    GPIO switch1;
    GPIO switch2;
    char buff[50];

    ls7366r_open(&decoder, "/dev/spidev1.0", "/sys/class/gpio/gpio10/value");
    
    snprintf(buff, 50, "/sys/class/gpio/gpio%d/value", GPIO_DEC_EN);
    ggpio_open(&decoder_en, buff, WRITE_ONLY);

    gpwm_open(&motor, PWM_MOTOR);
    snprintf(buff, 50, "/sys/class/gpio/gpio%d/value", GPIO_MOTOR_EN);
    ggpio_open(&motor_en, buff, WRITE_ONLY);

    snprintf(buff, 50, "/sys/class/gpio/gpio%d/value", GPIO_SW1);
    ggpio_open(&switch1, buff, READ_ONLY);

    snprintf(buff, 50, "/sys/class/gpio/gpio%d/value", GPIO_SW2);
    ggpio_open(&switch2, buff, READ_ONLY);

    quanser->decoder = decoder;
    quanser->decoder_en = decoder_en;
    quanser->motor = motor;
    quanser->motor_en = motor_en;
    quanser->switch1 = switch1;
    quanser->switch2 = switch2;
    return 0;
}

float quanser_read_encoder_rad(QUANSER quanser)
{
    int counter;
    float radians;

    counter = ls7366r_read_counter(quanser.decoder)/4;
    radians = 2 * PI * counter / 4096;

    return radians;
}

int quanser_enable_motor(QUANSER quanser, int enable)
{
    if(enable != GPIO_HIGH && enable != GPIO_LOW) return -1;

    if(ggpio_write(quanser.motor_en, enable) < 0) return -1;

    if(enable == GPIO_HIGH) {
        gpwm_enable(quanser.motor);
    } else {
        gpwm_disable(quanser.motor);
    }
    
    return 0;
}

int quanser_enable_decoder(QUANSER quanser, int enable)
{
    if(enable != GPIO_HIGH && enable != GPIO_LOW) return -1;

    if(ggpio_write(quanser.decoder_en, enable) < 0) return -1;

    return 0;
}

int quanser_set_motor_volts(QUANSER quanser, float volts)
{
    if(volts > 27 || volts < -27) {
        printf("Invalid volt value.\n");
        return -1;
    }
    int dc = (int) (volts + 27)*100/54;
    gpwm_write_dc_percent(quanser.motor, dc);

    return 0;
}

int quanser_read_switch(QUANSER quanser, int which)
{
    if(which == SWITCH1) return ggpio_read(quanser.switch1);
    if(which == SWITCH2) return ggpio_read(quanser.switch2);
    
    printf("Invalid switch.\n");
    return -1;
}

int quanser_close(QUANSER quanser)
{
    ls7366r_close(quanser.decoder);

    ggpio_write(quanser.decoder_en, GPIO_LOW);
    ggpio_close(quanser.decoder_en);

    ggpio_write(quanser.motor_en, GPIO_LOW);
    ggpio_close(quanser.motor_en);

    ggpio_close(quanser.switch1);
    ggpio_close(quanser.switch2);

    gpwm_close(quanser.motor);

    return 0;
}
