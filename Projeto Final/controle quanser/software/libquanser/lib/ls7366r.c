#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include <ls7366r.h>

int ls7366r_open(DECODER *decoder, char *spidev, char *ssgpio)
{
	decoder->fd_devspi = open(spidev, O_RDWR);
	decoder->fd_ss = open(ssgpio, O_WRONLY);

	if(decoder->fd_devspi < 0) {
		printf("Error open spidev.\n");
		exit(-1);
	}
	
	if(decoder->fd_ss < 0) {
		printf("Error open SS gpio.\n");
		exit(-1);
	}

	//ls7366r_setup_qnsr((*decoder));

	return 0;
}

int ls7366r_setup_qnsr(DECODER decoder)
{
	uint8_t mode = SPI_MODE_0;
	uint8_t lsb = 0;
	uint8_t bpw = 8;
	uint32_t rate = SPI_FREQ;
	unsigned char mdr0, mdr1;

	if(ioctl(decoder.fd_devspi, SPI_IOC_WR_MODE, &mode) < 0){
		printf("Error setting SPI write mode.\n");
		return -1;
	}

	if(ioctl(decoder.fd_devspi, SPI_IOC_WR_LSB_FIRST, &lsb)){
		printf("Error setting SPI LSB first.\n");
		return -1;
	}

	if(ioctl(decoder.fd_devspi, SPI_IOC_WR_BITS_PER_WORD, &bpw)){
		printf("Error setting SPI bits per word.\n");
		return -1;
	}

	if(ioctl(decoder.fd_devspi, SPI_IOC_WR_MAX_SPEED_HZ, &rate)){
		printf("Error setting SPI max speed.\n");
		return -1;
	}
	
	mdr0 = QUADRX4|FREE_RUN|DISABLE_INDX|FILTER_2;
	mdr1 = BYTE_2|EN_CNTR|NO_FLAGS;
	if(ls7366r_single_byte_write(decoder, WRITE_MDR0, mdr0) < 0){
		printf("Error in WRITE_MDR0.\n");
		return -1;
	}

	if(ls7366r_single_byte_write(decoder, WRITE_MDR1, mdr1)){
		printf("Error in WRITE_MDR1.\n");
		return -1;
	}

	// LS7366R_init(decoder.fd_devspi);

	return 0;
}

int ls7366r_single_byte_write(DECODER decoder, unsigned char opcode, unsigned char data)
{
    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS HIGH\n");
        return -1;
    }
    // slave select low
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "0", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    // send command
    if(write(decoder.fd_devspi, &opcode, sizeof opcode) < 0) {
        printf("Can't write to decoder.\n");
        return -1;
    }
    // send data
    if(write(decoder.fd_devspi, &data, sizeof data) < 0) {
        printf("Can't write to decoder.\n");
        return -1;
    }

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS HIGH\n");
        return -1;
    }

    return 0;
}

unsigned char ls7366r_single_byte_read(DECODER decoder, unsigned char opcode)
{
    unsigned char byte;

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS HIGH\n");
        return -1;
    }
    // slave select low
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "0", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    // send command
    if(write(decoder.fd_devspi, &opcode, sizeof opcode) < 0) {
        printf("Can't write to decoder.\n");
        return -1;
    }
    // read data
    lseek(decoder.fd_devspi, 0, SEEK_SET);
	while(read(decoder.fd_devspi, &byte, 1) != 1);

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    return byte;
}

int ls7366r_read_counter(DECODER decoder)
{
    unsigned char lsbyte, msbyte;
    int counter;
    char read_op = READ_CNTR;

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS HIGH\n");
        return -1;
    }
    // slave select low
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "0", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    // send command to read counter
    if(write(decoder.fd_devspi, &read_op, 1) < 0) {
        printf("Can't write to decoder.\n");
        return -1;
    }

    // read most significant byte
    lseek(decoder.fd_devspi, 0, SEEK_SET);
	while(read(decoder.fd_devspi, &msbyte, 1) != 1);
    // read most significant byte
    lseek(decoder.fd_devspi, 0, SEEK_SET);
	while(read(decoder.fd_devspi, &lsbyte, 1) != 1);

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    counter = (msbyte << 8) | lsbyte;
    return counter;
}

int ls7366r_rst_counter(DECODER decoder)
{
    unsigned char opcode = CLR_CNTR;

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS HIGH\n");
        return -1;
    }
    // slave select low
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "0", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    if(write(decoder.fd_devspi, &opcode, sizeof opcode) < 0){
        printf("Error reseting counter.\n");
        return -1;
    }

    // slave select high
    lseek(decoder.fd_ss, 0, SEEK_SET);
    if(write(decoder.fd_ss, "1", 1) < 0) {
        printf("Can't set SS LOW\n");
        return -1;
    }

    return 0;
}

int ls7366r_close(DECODER decoder)
{
    if(close(decoder.fd_devspi) < 0) return -1;
    if(close(decoder.fd_ss) < 0) return -1;

    return 0;
}