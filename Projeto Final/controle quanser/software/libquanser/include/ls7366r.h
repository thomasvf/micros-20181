#ifndef LS7366R_H
#define LS7366R_H

/* MDR0 configuration data */
//Count modes 
#define NQUAD 0x00                //non-quadrature mode 
#define QUADRX1 0x01              //X1 quadrature mode 
#define QUADRX2 0x02              //X2 quadrature mode 
#define QUADRX4 0x03              //X4 quadrature mode 

//Running modes 
#define FREE_RUN 0x00 
#define SINGE_CYCLE 0x04 
#define RANGE_LIMIT 0x08 
#define MODULO_N 0x0C 

//Index modes 
#define DISABLE_INDX 0x00         //index_disabled 
#define INDX_LOADC 0x10           //index_load_CNTR 
#define INDX_RESETC 0x20          //index_rest_CNTR 
#define INDX_LOADO 0x30           //index_load_OL 
#define ASYNCH_INDX 0x00          //asynchronous index 
#define SYNCH_INDX 0x80           //synchronous index 

//Clock filter modes 
#define FILTER_1 0x00           //filter clock frequncy division factor 1 
#define FILTER_2 0x80           //filter clock frequncy division factor 2 

/* MDR1 configuration data */
//Flag modes 
#define NO_FLAGS 0x00
#define IDX_FLAG 0x10
#define CMP_FLAG 0x20
#define BW_FLAG 0x40
#define CY_FLAG 0x80
    
//1 to 4 bytes data-width 
#define BYTE_4 0x00                  //four byte mode           
#define BYTE_3 0x01                  //three byte mode           
#define BYTE_2 0x02                  //two byte mode           
#define BYTE_1 0x03                  //one byte mode  

//Enable/disable counter  
#define EN_CNTR 0x00               //counting enabled 
#define DIS_CNTR 0x04              //counting disabled 

/* LS7366R op-code list */ 
#define CLR_MDR0 0x08    
#define CLR_MDR1 0x10 
#define CLR_CNTR 0x20 
#define CLR_STR 0x30 
#define READ_MDR0 0x48 
#define READ_MDR1 0x50 

#define READ_CNTR   0x60 
#define READ_OTR    0x68 
#define READ_STR    0x70 
#define WRITE_MDR1 0x90 
#define WRITE_MDR0 0x88 
#define WRITE_DTR   0x98 
#define LOAD_CNTR   0xE0    
#define LOAD_OTR    0xE4    

#define SPI_FREQ 5000000

typedef struct st_decoder {
    int fd_devspi;
    int fd_ss;
} DECODER;

/**
   Open spidev and ssgpio files and set cnofigurations for use with quanser.
 **/
int ls7366r_open(DECODER *decoder, char *spidev, char *ssgpio);

/**
   Set configurations of decoder to be used with Quanser.
   Return 0 if success and -1 otherwise.
 **/
int ls7366r_setup_qnsr(DECODER decoder);

/**
   Return byte read in operation _opcode_ applied in _decoder_.
 **/
unsigned char ls7366r_single_byte_read(DECODER decoder, unsigned char opcode);

/**
     Return current value of counter.
 **/
int ls7366r_read_counter(DECODER decoder);

/**
    Write command _opcode_ followed by _data_ to ls7366r described in _decoder_.
    Return 0 if success and -1 otherwise.
 **/
int ls7366r_single_byte_write(DECODER decoder, unsigned char opcode, unsigned char data);

/**
    Reset counter value of ls7366r described in _decoder_.
    Return 0 if success and -1 otherwise.
 **/
int ls7366r_rst_counter(DECODER decoder);

/**
    Close file descriptors in _decoder_.
**/
int ls7366r_close(DECODER decoder);

#endif
