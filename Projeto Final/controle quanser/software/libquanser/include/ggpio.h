#ifndef GGPIO_H
#define GGPIO_H

#define GPIO_HIGH 1
#define GPIO_LOW 0

#define READ_ONLY 0
#define WRITE_ONLY 1
#define READ_WRITE 2

typedef struct st_gpio {
    int fd;
    int mode;
} GPIO;

int ggpio_open(GPIO *gpio, char *file, int mode);

int ggpio_write(GPIO gpio, int value);

int ggpio_read(GPIO gpio);

int ggpio_close(GPIO gpio);

#endif
