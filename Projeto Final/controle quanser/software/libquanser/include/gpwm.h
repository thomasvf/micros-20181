#ifndef GPWM_H
#define GPWM_H

#define PWM_MAX_PERIOD 41600000
#define PWM_MIN_PERIOD 656000
#define PWM_DEFAULT_PERIOD 1000000

typedef struct st_pwm {
    int fd_enable;
    int fd_duty_cycle;
} PWM;

/**
    Open file descriptor in PWM _which_ and disable _pwm_.
    Return 0 if success and -1 otherwise.
**/
int gpwm_open(PWM *pwm, int which);

/**
    Write _duty_cycle_ to _pwm_.
    Return 0 if success and -1 otherwise.
**/
int gpwm_write_duty_cycle(PWM pwm, int duty_cycle);

/**
    Write duty cycle in _pwm_ as _percent_ percent of current pwm period.
    Return 0 if success and -1 otherwise.
**/
int gpwm_write_dc_percent(PWM pwm, int percent);

/**
    Set pwm period to _pwm_period.
    Return 0 if success and -1 otherwise.
**/
int gpwm_write_pwm_period(int pwm_period);

/**
    Enable _pwm_.
    Return 0 if success and -1 otherwise.
**/
int gpwm_enable(PWM pwm);

/**
    Disable _pwm_.
    Return 0 if success and -1 otherwise.
**/
int gpwm_disable(PWM pwm);

/**
    Disable _pwm_ and close file descriptors used in _pwm_.
    Return 0 if success and -1 otherwise.
**/
int gpwm_close(PWM pwm);

#endif
