#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "quanser.h"
#include "ggpio.h"
#include "gpwm.h"
#include "ls7366r.h"

int test_ls()
{
	unsigned char mode1r, mode2r;

	DECODER decoder;
    ls7366r_open(&decoder, "/dev/spidev1.0", "/sys/class/gpio/gpio10/value");

	mode1r = ls7366r_single_byte_read(decoder, READ_MDR0);
	mode2r = ls7366r_single_byte_read(decoder, READ_MDR1);
	printf("mode1r: %d mode2r: %d\n", mode1r, mode2r);

	ls7366r_rst_counter(decoder);

	while(1) {
		printf("Counter: %d\n", ls7366r_read_counter(decoder)/4);
		usleep(1000000);
	}

	return 0;
}

/**
    Test reading decoder through quanser interface.
**/
int test_ls_quanser()
{
    unsigned char mode1r, mode2r;
    int i;
    QUANSER quanser;
    quanser_open(&quanser);

    quanser_enable_decoder(quanser, GPIO_HIGH);
    ls7366r_setup_qnsr(quanser.decoder);

    mode1r = ls7366r_single_byte_read(quanser.decoder, READ_MDR0);
    mode2r = ls7366r_single_byte_read(quanser.decoder, READ_MDR1);
	printf("mode1r: %d mode2r: %d\n", mode1r, mode2r);

    ls7366r_rst_counter(quanser.decoder);
    for(i = 0; i < 10; i++) {
        printf("Counter: %f\n", quanser_read_encoder_rad(quanser));

        usleep(500000);
    }

    quanser_enable_decoder(quanser, GPIO_LOW);
    quanser_close(quanser);

    return 0;
}

int test_switch1()
{
    // gpio14 io3
    QUANSER quanser;
    quanser_open(&quanser);

    usleep(1000000);
    printf("Value switch1: %d\n", quanser_read_switch(quanser, SWITCH1));

    quanser_close(quanser);
    return 0;
}

int test_switch2()
{
    // gpio14 io3
    QUANSER quanser;
    quanser_open(&quanser);

    usleep(1000000);
    printf("Value switch2: %d\n", quanser_read_switch(quanser, SWITCH2));

    quanser_close(quanser);
    return 0;
}

int test_switch1_loop()
{
    QUANSER quanser;
    quanser_open(&quanser);

    while(1) {
        if(quanser_read_switch(quanser, SWITCH1) != 1) {
            printf("Switch1 is 0.\n");
            usleep(200000);
        } else {
            break;
        }
    }
    printf("Value switch1: %d\n", quanser_read_switch(quanser, SWITCH1));

    quanser_close(quanser);
    return 0;
}

int test_switch2_loop()
{
    QUANSER quanser;
    quanser_open(&quanser);

    while(1) {
        if(quanser_read_switch(quanser, SWITCH2) != 1) {
            printf("Switch2 is 0.\n");
            usleep(200000);
        } else {
            break;
        }
    }
    printf("Value switch2: %d\n", quanser_read_switch(quanser, SWITCH2));

    quanser_close(quanser);
    return 0;
}

/**
    Test enable pins while using the decoder.
**/
int test_quanser1()
{
    QUANSER quanser;
    quanser_open(&quanser);

	unsigned char mode1r, mode2r;

    quanser_enable_decoder(quanser, GPIO_HIGH);
    ls7366r_setup_qnsr(quanser.decoder);

	mode1r = ls7366r_single_byte_read(quanser.decoder, READ_MDR0);
	mode2r = ls7366r_single_byte_read(quanser.decoder, READ_MDR1);
	printf("mode1r: %d mode2r: %d\n", mode1r, mode2r);

	ls7366r_rst_counter(quanser.decoder);
    int i = 0;
	while(1) {
		printf("Radians from starting point: %f\n", quanser_read_encoder_rad(quanser));

        printf("Motor enabled.\n");
        quanser_enable_motor(quanser, GPIO_HIGH);
        ggpio_write(quanser.motor_en, GPIO_HIGH);

		usleep(500000);

        quanser_enable_motor(quanser, GPIO_LOW);
        printf("Motor disabled.\n");

        usleep(500000);
        
        i++;
        if(i > 7) {
            break;
        } else if(i > 5) {
            quanser_enable_decoder(quanser, GPIO_LOW);
        }
	}

    quanser_close(quanser);
	return 0;
}

/**
    Test all gpios used to control quanser.
**/
int test_qns_gpios()
{
    QUANSER quanser;
    quanser_open(&quanser);

    int i = 0;
    while(i < 10) {
        ggpio_write(quanser.decoder_en, GPIO_HIGH);
        ggpio_write(quanser.motor_en, GPIO_LOW);
        printf("SWITCH 1: %d\n", ggpio_read(quanser.switch1));
        printf("SWITCH 2: %d\n", ggpio_read(quanser.switch2));
        
        usleep(1000000);

        ggpio_write(quanser.decoder_en, GPIO_LOW);
        ggpio_write(quanser.motor_en, GPIO_HIGH);
        printf("SWITCH 1: %d\n", ggpio_read(quanser.switch1));
        printf("SWITCH 2: %d\n", ggpio_read(quanser.switch2));

        usleep(1000000);
        i++;
    }

    quanser_close(quanser);

    return 0;
}

int test_shield_ls()
{
    QUANSER quanser;
    unsigned char mode1r, mode2r;

    quanser_open(&quanser);

	mode1r = ls7366r_single_byte_read(quanser.decoder, READ_MDR0);
	mode2r = ls7366r_single_byte_read(quanser.decoder, READ_MDR1);
	printf("mode1r: %d mode2r: %d\n", mode1r, mode2r);

    int i=0;
    ls7366r_rst_counter(quanser.decoder);
    while(i < 5) {
        printf("Radians from starting point: %f\n", quanser_read_encoder_rad(quanser));

        usleep(1000000);
        i++;
    }
    quanser_close(quanser);

    return 0;
}

/**
    Slowly bring motor home.
**/
int test_motor_home()
{
    QUANSER quanser;
    quanser_open(&quanser);

    quanser_set_motor_volts(quanser, 1);
    quanser_enable_motor(quanser, GPIO_HIGH);

    usleep(1000000);
    while(quanser_read_switch(quanser, SWITCH1) != 1 && quanser_read_switch(quanser, SWITCH2) != 1){
        usleep(1000);
    }
    quanser_enable_motor(quanser, GPIO_LOW);

    quanser_close(quanser);
    return 0;
}

/**
    Test all values of voltages in the PWM motor outputs. From -27 to 27.
    This is not supposed to be tested with the motor.
**/
int test_pwm_motor()
{
    QUANSER quanser;
    int i;

    quanser_open(&quanser);

    quanser_set_motor_volts(quanser, 0);
    quanser_enable_motor(quanser, GPIO_HIGH);
    usleep(1000000);
    for(i = -27; i < 27; i++) {
        quanser_set_motor_volts(quanser, i);
        usleep(500000);
    }
    
    quanser_set_motor_volts(quanser, 0);
    quanser_enable_motor(quanser, GPIO_LOW);

    quanser_close(quanser);

    return 0;
}

int main(int argc, char *argv[])
{

    // if(argc < 2) {
    //     printf("Invalid number of arguments.\n");
    // }

    // if(strcmp(argv[1], "motor_home") == 0){
    //     printf("Testing motor_home.\n");
    //     return test_motor_home();
    // } else if(strcmp(argv[1], "test_ls") == 0) {
    //     printf("Testing ls7366r.\n");
    //     return test_ls();
    // } else if(strcmp(argv[1], "test_shield_ls") == 0) {
    //     printf("Testing ls7366r in shield.\n");
    //     return test_shield_ls();
    // } else if(strcmp(argv[1], "test_pwm_motor") == 0) {
    //     printf("Testing test_pwm_motor.\n");
    //     return test_pwm_motor();
    // } else if(strcmp(argv[1], "test_qns_gpios") == 0) {
    //     printf("Testing test_qns_gpios.\n");
    //     return test_qns_gpios();
    // } else if(strcmp(argv[1], "test_ls_quanser") == 0) {
    //     printf("Testing test_ls_quanser.\n");
    //     return test_ls_quanser();
    // } else if(strcmp(argv[1], "test_quanser1") == 0) {
    //     printf("Testing test_quanser1.\n");
    //     return test_quanser1();
    // }
    
    test_switch2_loop();

    return 0;
}