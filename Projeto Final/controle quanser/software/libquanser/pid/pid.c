#include <pid.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

lista* amostras;
pid_struct* PID_struct;

void initPID()
{
	PID_struct = (pid_struct*) malloc(sizeof(pid_struct));
}

void initSamples(lista* novo)
{
	amostras = novo;
}

void cleanSamples()
{
	free(amostras);
}

void addSample(long time, double value)
{
	lista *novo;
	lista *atual;
	novo = (lista*) malloc(sizeof(lista));
	novo->value = value;
	novo->timestamp = time;
	atual = amostras;
	if(atual == NULL)
		initSamples(novo);
	else
		while (atual)
		{
			if(atual->next == NULL)
			{
				atual->next = novo;
				break;
			}
			else
				atual = atual->next;
		}		
}

lista* getLastSample()
{
	lista *atual = amostras;
	while (atual)
	{
		if(atual->next == NULL)
			return atual;
		else
			atual = atual->next;
	}
	return NULL;
}

void bumplessTransfer(QUANSER quanser)
{
	quanser_enable_decoder(quanser, GPIO_HIGH);
	//COb = CO
	PID_struct->COb = 10;
	//SP = PV
	//Transforma o setpoint no predicted value lido pelo SPI
	PID_struct->SP = quanser_read_encoder_rad(quanser);
	PID_struct->PV = PID_struct->SP;
}

void bumpTest(QUANSER quanser)
{
	double currentPV = 0;
	double currentCO = 0;
	long startTime, middleTime;

	if(PID_struct == NULL)
		initPID(PID_struct);
	if(amostras == NULL)
		initSamples(amostras);

	//Alterar para valores correspondentes
	if(PID_struct->SP > 0)
		currentCO = 10;
	else if(PID_struct->SP == 0)
		currentCO = 0;
	else
		currentCO = -10;
	
	
	//O controlador envia um sinal na amplitude do objetivo
	quanser_enable_motor(quanser, GPIO_HIGH);
	quanser_set_motor_volts(quanser, currentCO);
	
	//O controlador monitora valuees de PV (armazenando num array?) até que fique estável
	//--Ou seja, até que PV[x] - PV[x-1] < epsilon
	//Essa função também retorna value de início de resposta
	monitorBump(&startTime, &currentPV, quanser);
		
	//Calcula o ganho do processo com base na variação de PV e CO
	PID_struct->Kp = (currentPV - PID_struct->PV) / (currentCO - PID_struct->CO);

	//Calcula a constante de tempo do processo, com base na variação de PV
	currentPV = PID_struct->PV - 0.63 * (currentPV - PID_struct->PV);

	//Calcula o tempo correspondente a 63% (busca no array)
	middleTime = getTimeAt(currentPV);

	//Calcula variação entre início da resposta e os 63% (PT)
	PID_struct->Tp = middleTime - startTime;

	//Aproximação moderada
	PID_struct->Tc = PID_struct->Tp;
	//Aproximação agressiva
	//PID.Tc = 0.1 * PID.Tp;
	//Aproximação conservadora
	//PID.Tc = 10 * PID.Tp;

	//Calcula Kc
	PID_struct->Kc = ((PID_struct->Tp + 0.5 * PID_struct->Thetap) / (PID_struct->Tc + 0.5 * PID_struct->Thetap)) * (1 / PID_struct->Kp);

	//Calcula Ti
	PID_struct->Ti = PID_struct->Tp + 0.5 * PID_struct->Thetap;

	//Calcula Td
	PID_struct->Td = (PID_struct->Tp * PID_struct->Thetap) / (2 * PID_struct->Tp + PID_struct->Thetap);
}


void monitorBump(long *startTime, double *currentPV, QUANSER quanser)
{
	double error = 1;
	lista *lastSample, *currentSample; //Amostra
	long startMonitoring = time(0); //Tempo do início do monitoramento
	//Verifica leituras do sensor do instante atual até que
	//ele se estabilize
	while(error > ERROR)
	{
		//Pega um sample via SPI
		currentSample = getNewSample(time(0), quanser);
		//Calcula a diferença entre o último value e o atual
		error = abs(lastSample->value - currentSample->value);
		lastSample = currentSample;
	}
	*startTime = amostras->timestamp;
	*currentPV = getLastSample()->value;
}	

long getTimeAt(double currentPV)
{
	//Lê o array de PVs e encontra instante da leitura mais
	//próxima do value calculado (currentPV)
	lista *atual;
	lista *maisProxima;
	double menorDiferenca = 0;
	atual = amostras;
	while(atual)
	{
		//Se for a primeira iteraçao, ou for menor que a menor diferença
		if(maisProxima == NULL || abs(atual->value - currentPV) < menorDiferenca)
		{
			menorDiferenca = abs(atual->value - currentPV);
			maisProxima = atual;
		}
		atual = atual->next;
	}
	return maisProxima->timestamp;	
}

void createRandomSamples()
{
	srand(time(NULL));   // should only be called once
	int i;
	for(i = 0; i < 10; i++)
	{
		addSample(time(NULL), rand());
		sleep(0.5);
	}
}

double calculatePID(QUANSER quanser)
{
	double p, i, d, CO;
	lista *lastSample = getLastSample();
	lista *currentSample = getNewSample(time(0), quanser);
	long dt = abs(lastSample->timestamp - currentSample->timestamp);

	PID_struct->error = PID_struct->SP - currentSample->value;

	p = PID_struct->error;
	i = PID_struct->integral + PID_struct->error * dt;
	d = (lastSample->value - currentSample->value) / dt;

	PID_struct->integral = i;

	CO = PID_struct->COb + PID_struct->Kc * p + (PID_struct->Kc / PID_struct->Ti) * i + (PID_struct->Kc * PID_struct->Td) * d;

	return CO;	
}

lista* getNewSample(long timestamp, QUANSER quanser)
{
	//Le um novo sample
	lista *sample, *last;
	sample = (lista*) malloc(sizeof(lista));
	sample->timestamp = timestamp;
	sample->value = quanser_read_encoder_rad(quanser);
	//Adiciona na lista
	last = getLastSample();
	last->next = sample;
	return sample;
}