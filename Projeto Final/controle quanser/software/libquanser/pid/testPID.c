#include "pid.h"
#include "quanser.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
	int i;
	/* Teste da lista */
	initPID();
	lista* atual = amostras;
	QUANSER quanser;
    quanser_open(&quanser);
	createRandomSamples();
	for(i = 0; i < 10; i++)
	{
		printf("Time: %ld\tValue: %.2f\n", atual->timestamp, atual->value);
		atual = atual->next;
	}
	cleanSamples();
	if(amostras == NULL)
		printf("Uhuul");
	return 0;
}
