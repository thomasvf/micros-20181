var searchData=
[
  ['lista',['lista',['../structlista.html',1,'lista'],['../pid_8h.html#a2dc8ffe00ae6dbb2065aa8a406c1981e',1,'lista():&#160;pid.h']]],
  ['load_5fcntr',['LOAD_CNTR',['../ls7366r_8h.html#acaf0f48f32bcf0364ceb6af9fe679811',1,'ls7366r.h']]],
  ['load_5fotr',['LOAD_OTR',['../ls7366r_8h.html#a3d58f007c5ba589531eeaec7c2328b3f',1,'ls7366r.h']]],
  ['ls7366r_2eh',['ls7366r.h',['../ls7366r_8h.html',1,'']]],
  ['ls7366r_5fclose',['ls7366r_close',['../ls7366r_8h.html#a915fcc16afdbef35c109739154ac710e',1,'ls7366r.h']]],
  ['ls7366r_5fopen',['ls7366r_open',['../ls7366r_8h.html#ace7dadb479e71984016dfa73a43f6b73',1,'ls7366r.h']]],
  ['ls7366r_5fread_5fcounter',['ls7366r_read_counter',['../ls7366r_8h.html#a46836267a48f1184e80a963168b20d05',1,'ls7366r.h']]],
  ['ls7366r_5frst_5fcounter',['ls7366r_rst_counter',['../ls7366r_8h.html#abf40b38918a2a80669bc6da75a5886c9',1,'ls7366r.h']]],
  ['ls7366r_5fsetup_5fqnsr',['ls7366r_setup_qnsr',['../ls7366r_8h.html#a5e98847937f63e4afdac43df20bdc003',1,'ls7366r.h']]],
  ['ls7366r_5fsingle_5fbyte_5fread',['ls7366r_single_byte_read',['../ls7366r_8h.html#ad8a1d05061441eb9f976231e4837073d',1,'ls7366r.h']]],
  ['ls7366r_5fsingle_5fbyte_5fwrite',['ls7366r_single_byte_write',['../ls7366r_8h.html#a6ae5178984f26ac9e7fe052db5258ba5',1,'ls7366r.h']]]
];
